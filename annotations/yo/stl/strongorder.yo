The class tt(std::strong_ordering)hi(strong_ordering) is used when
implementing the spaceship operator for classes that support all comparison
operators (where one operand may be zero), as well as substitutability.

The class tt(strong_ordering) provides free functions for all comparison
operations (tt(==, !=, <, <=, >,) and tt(>=)) expecting tt(partial_ordering)
arguments (one argument may be 0). It also defines three static objects which
can be returned by the spaceship operator:
    itemization(
    itt(strong_ordering::less), returned when the lhs operand of the
        spaceship operator should be ordered before the rhs operand;
    itt(strong_ordering::equal), indicating equality: there is no
        ordering preference between the two operands of the spaceship operator;
    itt(strong_ordering::greater), returned when the lhs operand of the
        spaceship operator should be ordered after the rhs operand;
    )

An example where the class tt(strong_ordering) was used has already been
provided in section ref(SPACESHIP), where the spaceship operator itself was
introduced.


